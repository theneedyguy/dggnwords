#!/bin/bash
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o nwords .
docker build . -t ckevi/dggnwordapi:latest
docker tag ckevi/dggnwordapi:latest ckevi/dggnwordapi:0.5
docker push ckevi/dggnwordapi:latest
docker push ckevi/dggnwordapi:0.5

