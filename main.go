package main


import (
	"net/http"
	"encoding/json"
	"log"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"regexp"
)

// The response struct
type response struct {
	R int `json:"r"`
	A int `json:"a"`
	Nick string `json:"nick"`
}

// Entrypoint to handle
func main() {
	http.HandleFunc("/count", func(res http.ResponseWriter, req *http.Request) {
		res.Header().Set("Content-Type", "application/json")
		counter(res, req)
	})
	log.Fatal(http.ListenAndServe(":1111", nil))
}

// Handles the /count endpoint for the http Server
// Returns a json object if everything succeeds
func counter(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		nick := req.FormValue("nick")
		var months []interface{} = getMonths()
		nwordsA, nwordsR := 0, 0
		for i := range months {
			var nA, nR = countNWord(getMonthLogs(months[i], nick))
			nwordsA	= nwordsA + nA
			nwordsR	= nwordsR + nR
		}
		log.Print(nwordsA, nwordsR)
	        a := &response{R: nwordsR, A: nwordsA, Nick: nick}
		out, err := json.Marshal(a)
		if err != nil {
			log.Fatalln(err)
		}
		io.WriteString(res, string(out))
	} else {
		res.WriteHeader(http.StatusForbidden)
	}
}

// Query the overrustle api for the months api
// Returns an interface slice
func getMonths() []interface {} {
	resp, err := http.Get("https://overrustlelogs.net/api/v1/Destinygg/months.json")
	if err != nil {
		log.Fatalln(err)
	}
	var result []interface{}
	json.NewDecoder(resp.Body).Decode(&result)
	return result
}

// Query a user's logs for a certain month
// Requires an interface and a string
// Returns a string slice
func getMonthLogs(month interface{}, user string) []string {
	resp, err := http.Get(fmt.Sprintf("https://overrustlelogs.net/Destinygg chatlog/%v/userlogs/%s.txt", month, user))
	if err != nil {
		log.Fatalln(err)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	    }
	bodyString := string(bodyBytes)
	if strings.HasPrefix(bodyString, "didn't find any logs for this user") {
		return []string{}
	} else {
		lines := strings.Split(bodyString, "\n")
		return lines
	}
}

// Count the words with regex.
// Expects a slice of strings 
// Returns 2 integers. The first one is for the 'soft' n-word, the second for the hard one
func countNWord(lines []string) (int, int) {
	var reA = regexp.MustCompile(`(?i)\s?nigga\s?`)
	var reR = regexp.MustCompile(`(?i)\s?nigger\s?`)
	nwordsA, nwordsR := 0, 0
	for i := range lines {
		matchesA :=  reA.FindAllStringIndex(lines[i], -1)
		matchesR :=  reR.FindAllStringIndex(lines[i], -1)
		nwordsA = nwordsA + len(matchesA)
		nwordsR = nwordsR + len(matchesR)
	}
	return nwordsA, nwordsR

}
