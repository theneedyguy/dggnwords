FROM alpine:latest as alpine
RUN apk add -U --no-cache ca-certificates


FROM busybox
LABEL maintainer="CKEVI <admin@0x01.host>"
RUN mkdir -p /opt/nwords

COPY --from=alpine /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY nwords /opt/nwords/nwords

USER nobody
EXPOSE 1111
ENTRYPOINT [ "/opt/nwords/nwords" ]

